## About This Project

This is a test project for FlashBox company
- [FlashBox Website](https://flashbox.co/).


## What to do for using this project?


#### clone the project
#### copy .env.example to .env
#### composer install
#### php artisan key:generate
#### php artisan migrate
#### php artisan db:seed
#### php artisan admin:install


## To create customer use this command

#### php artisan customer:create

---------------------------------------------------------------------------------------------------

# Curls

curl --location --request POST 'localhost:8001/api/auth/login' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
"email" : "I9c49176@flashbox.com",
"password" : "123456",
"method" : "default"
}'



curl --location --request POST 'localhost:8001/api/admin/seller/store' \
--header 'Accept: application/json' \
--header 'Authorization: Bearer 9a15b980-4be2-11ec-9910-1b4591043c55' \
--header 'Content-Type: application/json' \
--data-raw '{
"email" : "seller@flashbox.com",
"password" : "123456",
"address" : "tehran"
}'



curl --location --request POST 'localhost:8001/api/seller/product/store' \
--header 'Accept: application/json' \
--header 'Authorization: Bearer a7649270-4c3e-11ec-baf6-8727398a1151' \
--header 'Content-Type: application/json' \
--data-raw '{
"title" : "iphone 13",
"description" : "new released",
"price" : "50000",
"type" : "Digital"
}'



curl --location --request GET 'localhost:8001/api/products?address=tehran' \
--header 'Accept: application/json' \
--header 'Authorization: Bearer 30f0be30-4c61-11ec-8bc4-53ef1c3d3949'
