<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SellerController;

Route::group(
    ['prefix' => 'auth'],
    function ($router) {
        $router->post('login', [LoginController::class, 'login']);
    }
);

Route::group(
    ['prefix' => 'admin', 'middleware' => ['admin']],
    function ($router) {
        $router->post('seller/store', [AdminController::class, 'sellerStore']);
    }
);

Route::group(
    ['prefix' => 'seller', 'middleware' => ['seller']],
    function ($router) {
        $router->post('product/store', [SellerController::class, 'productStore']);
    }
);

Route::group(
    ['prefix' => '', 'middleware' => ['authentication']],
    function ($router) {
        $router->get('products', [ProductController::class, 'getProducts']);
    }
);
