<?php

namespace App\Console\Commands;

use App\Enums\RolesEnum;
use App\Repositories\Interfaces\RoleRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AdminInstallation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Admin Installation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param UserRepositoryInterface $userRepository
     * @param RoleRepositoryInterface $roleRepository
     *
     * @return int
     */
    public function handle(UserRepositoryInterface $userRepository, RoleRepositoryInterface $roleRepository)
    {
        $email = env('ADMIN_EMAIL', 'admin@flashbox.com');
        $password = env('ADMIN_PASSWORD', '123456');

        $checkUserExistence = $userRepository->checkUserExistence($email);

        if ($checkUserExistence) {
            echo "User already exists";
            echo "\n";
            echo "Email : " . $email;
            echo "\n";
            echo "Password : " . $password;
            echo "\n";

            return 0;
        }

        DB::transaction(function () use (&$userRepository, &$roleRepository, &$email, &$password) {
            $admin = $userRepository->createAdmin($email, getHashOfString($password));
            $adminRole = $roleRepository->getRoleByName(RolesEnum::ADMIN);
            $admin->roles()->attach($adminRole->id);
        });


        echo "Email : " . $email;
        echo "\n";
        echo "Password : " . $password;
        echo "\n";

        return 0;
    }
}
