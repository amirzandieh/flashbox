<?php

namespace App\Console\Commands;

use App\Enums\RolesEnum;
use App\Repositories\Interfaces\RoleRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CustomerGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Customer Generator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param UserRepositoryInterface $userRepository
     * @param RoleRepositoryInterface $roleRepository
     *
     * @return int
     */
    public function handle(UserRepositoryInterface $userRepository, RoleRepositoryInterface $roleRepository)
    {
        $email = Str::random(8) . '@flashbox.com';
        $password = env('ADMIN_PASSWORD', '123456');

        $checkUserExistence = $userRepository->checkUserExistence($email);

        if ($checkUserExistence) {
            echo "User already exists";
            echo "\n";
            echo "Email : " . $email;
            echo "\n";
            echo "Password : " . $password;
            echo "\n";

            return 0;
        }

        DB::transaction(function () use (&$userRepository, &$roleRepository, &$email, &$password) {
            $admin = $userRepository->create([
                'email' => $email,
                'password' => getHashOfString($password)
            ]);
            $adminRole = $roleRepository->getRoleByName(RolesEnum::CUSTOMER);
            $admin->roles()->attach($adminRole->id);
        });
        
        echo "Email : " . $email;
        echo "\n";
        echo "Password : " . $password;
        echo "\n";

        return 0;
    }
}
