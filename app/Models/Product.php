<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'product_type',
        'product_id',
    ];

    public function buyable()
    {
        return $this->morphTo(Product::class, 'product_type', 'product_id');
    }
}
