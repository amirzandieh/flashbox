<?php

namespace App\Constants;

class Tables
{
    public const USER = 'users';
    public const ROLE = 'roles';
    public const PERMISSION = 'permissions';
    public const PERMISSION_ROLE = 'permission_role';
    public const ROLE_USER = 'role_user';

    public const STORES = 'stores';

    public const PRODUCTS = 'products';
    public const DIGITAL_OBJECTS = 'digital_objects';
    public const HOME_OBJECTS = 'home_objects';

    public const ORDERS = 'orders';

    public const PAYMENT = 'payment';

    public const AUTH_TOKEN = 'auth_tokens';
}
