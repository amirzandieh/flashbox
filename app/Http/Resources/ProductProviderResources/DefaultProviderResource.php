<?php

namespace App\Http\Resources\ProductProviderResources;

use App\Services\Product\ProductProviders\ProductTransformerInterface;
use Illuminate\Http\Resources\Json\JsonResource;

class DefaultProviderResource extends JsonResource implements ProductTransformerInterface
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->resource;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->resource['title'];
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->resource['description'];
    }

    /**
     * @return string
     */
    public function price(): string
    {
        return $this->resource['price'];
    }
}
