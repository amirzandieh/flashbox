<?php

namespace App\Http\Requests;

use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email', 'exists:App\Models\User,email'],
            'password' => ['required', 'min:6']
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        /** @var UserRepositoryInterface $userRepository */
        $userRepository = app()->make(UserRepositoryInterface::class);

        $email = $this->email;

        if ($email) {
            $user = $userRepository->getUserViaEmail($this->email);

            $validator->after(function ($validator) use ($user) {
                if ( !Hash::check($this->password, $user->password) ) {
                    $validator->errors()->add('password', 'Your password is incorrect.');
                }
            });
        }

        return;
    }
}
