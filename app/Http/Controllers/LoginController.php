<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Resources\LoginResource;
use App\Services\Auth\AuthService;

class LoginController extends Controller
{
    /**
     * @param LoginRequest $request
     * @param AuthService $loginService
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request, AuthService $loginService): \Illuminate\Http\JsonResponse
    {
        $response = $loginService->loginProcess($request);

        return response()->json([
            'data' => new LoginResource($response)
        ]);
    }
}
