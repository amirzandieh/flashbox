<?php

namespace App\Http\Controllers;

use App\Http\Requests\SellerStoreRequest;
use App\Http\Resources\SellerStoreResource;
use App\Services\Admin\AdminService;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * @param Request $request
     * @param AdminService $adminService
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sellerStore(SellerStoreRequest $request, AdminService $adminService): \Illuminate\Http\JsonResponse
    {
        $user = $adminService->sellerStore($request);

        return response()->json([
            'data' => new SellerStoreResource($user)
        ]);
    }
}
