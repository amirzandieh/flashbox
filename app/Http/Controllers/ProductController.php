<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\ProductsRequest;
use App\Http\Resources\LoginResource;
use App\Services\Auth\AuthService;
use App\Services\Product\ProductService;

class ProductController extends Controller
{
    /**
     * @param ProductService $productService
     *
     * @return array
     */
    public function getProducts(ProductsRequest $request, ProductService $productService)
    {
        return $productService->getProducts($request);
    }
}
