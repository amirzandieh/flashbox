<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Resources\ProductStoreResource;
use App\Services\Product\ProductService;

class SellerController extends Controller
{
    /**
     * @param ProductStoreRequest $request
     * @param ProductService $productService
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function productStore(ProductStoreRequest $request, ProductService $productService): \Illuminate\Http\JsonResponse
    {
        $product = $productService->productStore($request);

        return response()->json([
            'data' => new ProductStoreResource($product)
        ]);
    }
}
