<?php

namespace App\Http\Middleware;

use App\Repositories\Interfaces\AuthTokenRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Closure;
use Illuminate\Http\Request;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var AuthTokenRepositoryInterface $authTokenRepositoryInterface */
        $authTokenRepositoryInterface = app()->make(AuthTokenRepositoryInterface::class);

        /** @var UserRepositoryInterface $userRepositoryInterface */
        $userRepositoryInterface = app()->make(UserRepositoryInterface::class);

        $token = $request->bearerToken();

        if (! $token) {
            abort(401);
        }

        $authToken = $authTokenRepositoryInterface->findByToken($token);

        if (! $authToken->exists) {
            abort(401);
        }

        $isAdmin = $userRepositoryInterface->isAdmin($authToken->user_id);

        if (! $isAdmin) {
            abort(403);
        }

        return $next($request);
    }
}
