<?php

namespace App\Http\Middleware;

use App\Repositories\Interfaces\AuthTokenRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Closure;
use Illuminate\Http\Request;

class AuthenticationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var AuthTokenRepositoryInterface $authTokenRepositoryInterface */
        $authTokenRepositoryInterface = app()->make(AuthTokenRepositoryInterface::class);

        $token = $request->bearerToken();

        if (! $token) {
            abort(401);
        }

        $authToken = $authTokenRepositoryInterface->findByToken($token);

        if (! $authToken->exists) {
            abort(401);
        }

        return $next($request);
    }
}
