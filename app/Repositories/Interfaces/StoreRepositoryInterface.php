<?php

namespace App\Repositories\Interfaces;

use App\Models\Store;

interface StoreRepositoryInterface
{
    /**
     * @param int $sellerId
     *
     * @return Store
     */
    public function getStoreBySellerId(int $sellerId): Store;

    /**
     * @param string $lat
     * @param string $long
     *
     * @return array
     */
    public function getStoreIdsWithGeoParams(string $lat, string $long): array;
}

