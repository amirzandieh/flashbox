<?php

namespace App\Repositories\Interfaces;

use App\Models\User;

interface UserRepositoryInterface
{
    /**
     * @param string $email
     * @param string $password
     *
     * @return User
     */
    public function createAdmin(string $email, string $password): User;

    /**
     * @param string $email
     *
     * @return bool
     */
    public function checkUserExistence(string $email): bool;

    /**
     * @param string $email
     *
     * @return User
     */
    public function getUserViaEmail(string $email): User;

    /**
     * @param int $userId
     *
     * @return bool
     */
    public function isAdmin(int $userId): bool;

    /**
     * @param int $userId
     *
     * @return bool
     */
    public function isSeller(int $userId): bool;
}
