<?php

namespace App\Repositories\Interfaces;

interface BaseRepositoryInterface
{
    /**
     * @param int $id
     * @return mixed
     */
    public function find(int $id);

    /**
     * @param array $attributes
     *
     * @return mixed
     */
    public function create(array $attributes);
}
