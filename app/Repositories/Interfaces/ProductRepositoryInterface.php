<?php

namespace App\Repositories\Interfaces;

interface ProductRepositoryInterface
{
    /**
     * @param string $title
     * @param string $description
     * @param string $price
     * @param int $storeId
     *
     * @return mixed
     */
    public function createDigitalObjectProduct(string $title, string $description, string $price, int $storeId);

    /**
     * @param string $title
     * @param string $description
     * @param string $price
     * @param int $storeId
     *
     * @return mixed
     */
    public function createHomeObjectProduct(string $title, string $description, string $price, int $storeId);
}
