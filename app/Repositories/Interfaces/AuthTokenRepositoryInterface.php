<?php

namespace App\Repositories\Interfaces;

use App\Models\AuthToken;

interface AuthTokenRepositoryInterface
{
    /**
     * @param string $token
     *
     * @return AuthToken
     */
    public function findByToken(string $token): AuthToken;
}
