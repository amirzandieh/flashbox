<?php

namespace App\Repositories\Interfaces;

use App\Models\Role;

interface RoleRepositoryInterface
{
    /**
     * @param string $title
     * @param string $description
     *
     * @return Role
     */
    public function createRole(string $title, string $description): Role;

    /**
     * @param string $name
     *
     * @return Role
     */
    public function getRoleByName(string $name): Role;
}
