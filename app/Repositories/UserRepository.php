<?php

namespace App\Repositories;

use App\Enums\RolesEnum;
use App\Models\User;
use App\Repositories\Interfaces\BaseRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;

class UserRepository extends BaseRepository implements UserRepositoryInterface, BaseRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return User
     */
    public function createAdmin(string $email, string $password): User
    {
        return $this->create([
           'email' => $email,
           'password' => $password
        ]) ?? $this->model;
    }

    /**
     * @param string $email
     *
     * @return bool
     */
    public function checkUserExistence(string $email): bool
    {
        return $this->model->where('email', $email)->count();
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public function getUserViaEmail(string $email): User
    {
        return $this->model
            ->where('email', $email)
            ->first() ?? $this->model;
    }

    /**
     * @param int $userId
     *
     * @return bool
     */
    public function isAdmin(int $userId): bool
    {
        $user = $this->model->find($userId) ?? $this->model;

        if (! $user->exists) {
            return false;
        }

        foreach ($user->roles()->get() as $role)
        {
            if ($role->title == RolesEnum::ADMIN)
            {
                return true;
            }
        }

        return false;
    }

    /**
     * @param int $userId
     *
     * @return bool
     */
    public function isSeller(int $userId): bool
    {
        $user = $this->model->find($userId) ?? $this->model;

        if (! $user->exists) {
            return false;
        }

        foreach ($user->roles()->get() as $role)
        {
            if ($role->title == RolesEnum::SELLER)
            {
                return true;
            }
        }

        return false;
    }
}
