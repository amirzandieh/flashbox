<?php

namespace App\Repositories;

class BaseRepository
{
    /**
     * @var mixed $model
     */
    protected $model;

    /**
     * @param int $id
     * @return mixed
     */
    public function find(int $id)
    {
        return $this->model->find($id);
    }

    /**
     * @param array $attributes
     *
     * @return mixed
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }
}
