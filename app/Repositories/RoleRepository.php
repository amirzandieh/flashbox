<?php

namespace App\Repositories;

use App\Enums\RolesEnum;
use App\Models\Role;
use App\Repositories\Interfaces\BaseRepositoryInterface;
use App\Repositories\Interfaces\RoleRepositoryInterface;

class RoleRepository extends BaseRepository implements RoleRepositoryInterface, BaseRepositoryInterface
{
    /**
     * RoleRepository constructor.
     *
     * @param Role $role
     */
    public function __construct(Role $role)
    {
        $this->model = $role;
    }

    /**
     * @param string $title
     * @param string $description
     *
     * @return Role
     */
    public function createRole(string $title, string $description): Role
    {
        return $this->create([
                'title' => $title,
                'description' => $description
            ]) ?? $this->model;
    }

    /**
     * @param string $name
     *
     * @return Role
     */
    public function getRoleByName(string $name): Role
    {
        return $this->model
            ->where('title', $name)
            ->first();
    }
}
