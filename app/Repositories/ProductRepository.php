<?php

namespace App\Repositories;

use App\Enums\RolesEnum;
use App\Models\DigitalObject;
use App\Models\Product;
use App\Models\User;
use App\Repositories\Interfaces\BaseRepositoryInterface;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Facades\DB;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface, BaseRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    /**
     * @param string $title
     * @param string $description
     * @param string $price
     * @param int $storeId
     *
     * @return mixed
     */
    public function createDigitalObjectProduct(string $title, string $description, string $price, int $storeId)
    {
        $object = DigitalObject::create([
            'title' => $title,
            'description' => $description,
            'store_id' => $storeId,
            'price' => $price,
            'tax' => '2000',
        ]);

        $this->model
            ->create([
                'product_type' => DigitalObject::class,
                'product_id' => $object->id
            ]);

        return $object;
    }

    /**
     * @param string $title
     * @param string $description
     * @param string $price
     * @param int $storeId
     *
     * @return mixed
     */
    public function createHomeObjectProduct(string $title, string $description, string $price, int $storeId)
    {
        // TODO: Implement createHomeObjectProduct() method.
    }

    /**
     * @param array $storeIds
     *
     * @return array
     */
    public function getProducts(array $storeIds): array
    {
        return $this->model
            ->whereIn('store_id', $storeIds)
            ->get()
            ->toArray();
    }
}
