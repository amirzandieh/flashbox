<?php

namespace App\Repositories;

use App\Models\Store;
use App\Repositories\Interfaces\BaseRepositoryInterface;
use App\Repositories\Interfaces\StoreRepositoryInterface;
use Illuminate\Support\Facades\DB;

class StoreRepository extends BaseRepository implements StoreRepositoryInterface, BaseRepositoryInterface
{
    /**
     * StoreRepository constructor.
     *
     * @param Store $store
     */
    public function __construct(Store $store)
    {
        $this->model = $store;
    }

    /**
     * @param int $sellerId
     *
     * @return Store
     */
    public function getStoreBySellerId(int $sellerId): Store
    {
        return $this->model
            ->where('seller_id', $sellerId)
            ->first() ?? $this->model;
    }

    /**
     * @param string $lat
     * @param string $long
     *
     * @return array
     */
    public function getStoreIdsWithGeoParams(string $lat, string $long): array
    {
        try {
            $results = DB::select(DB::raw('SELECT id, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( long ) - radians(' . $long . ') ) + sin( radians(' . $lat .') ) * sin( radians(lat) ) ) ) AS distance FROM stores HAVING distance < ' . 20 . ' ORDER BY distance') );
        } catch (\Exception $exception) {
            $results = [];
        }

        return $results;
    }
}
