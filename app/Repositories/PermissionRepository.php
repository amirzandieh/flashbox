<?php

namespace App\Repositories;

use App\Enums\RolesEnum;
use App\Models\User;
use App\Repositories\Interfaces\BaseRepositoryInterface;
use App\Repositories\Interfaces\PermissionRepositoryInterface;
use App\Repositories\Interfaces\RoleRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;

class PermissionRepository extends BaseRepository implements PermissionRepositoryInterface, BaseRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }
}
