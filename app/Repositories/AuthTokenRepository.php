<?php

namespace App\Repositories;

use App\Enums\RolesEnum;
use App\Models\AuthToken;
use App\Models\User;
use App\Repositories\Interfaces\AuthTokenRepositoryInterface;
use App\Repositories\Interfaces\BaseRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;

class AuthTokenRepository extends BaseRepository implements AuthTokenRepositoryInterface, BaseRepositoryInterface
{
    /**
     * AuthTokenRepository constructor.
     *
     * @param AuthToken $authToken
     */
    public function __construct(AuthToken $authToken)
    {
        $this->model = $authToken;
    }

    /**
     * @param string $token
     *
     * @return AuthToken
     */
    public function findByToken(string $token): AuthToken
    {
        return $this->model
            ->where('token', $token)
            ->first() ?? $this->model;
    }
}
