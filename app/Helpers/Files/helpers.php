<?php

use Illuminate\Support\Facades\Hash;
use Webpatser\Uuid\Uuid;

if (!function_exists('getHashOfString')) {
    /**
     * @return string
     */
    function getHashOfString(string $word): string
    {
        return Hash::make($word);
    }
}

if (!function_exists('tokenGenerator')) {
    /**
     * @return string
     * @throws Exception
     */
    function tokenGenerator()
    {
        return Uuid::generate()->string;
    }
}
