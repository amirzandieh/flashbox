<?php

namespace App\Services\Product;

use App\Models\Store;
use App\Models\User;
use App\Repositories\Interfaces\AuthTokenRepositoryInterface;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Repositories\Interfaces\StoreRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\Geocode\Facades\Geocode;
use App\Services\Product\ProductProviders\Providers\DefaultProvider;
use App\Services\Product\ProductProviders\Providers\DigikalaProvider;
use App\Services\Product\ProductProviders\Providers\TimcheProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductService
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var User
     */
    protected $authUser;

    /**
     * @var Store
     */
    protected $store;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
        $this->init();
    }

    /**
     * @param Request $request
     *
     * @return Model
     */
    public function productStore(Request $request): Model
    {
        $title = $request->get('title');
        $description = $request->get('description');
        $price = $request->get('price');
        $type = $request->get('type');

        $product = DB::transaction(function () use (&$title, &$description, &$price, &$type) {
            return $this->productRepository->{'create' . $type . 'ObjectProduct'}($title, $description, $price, $this->store->id);
        });

        return $product;
    }


    /**
     * @param Request $request
     *
     * @return array
     */
    public function getProducts(Request $request): array
    {
        $address = $request->get('address');
        $geocode = Geocode::geocode($address);
        $lat = $geocode['lat'];
        $long = $geocode['long'];

//        These providers should come from database or something like that to handle them from admin panel
        $serviceProviders = [
            DefaultProvider::class => env('DEFAULT_SERVICE_PROVIDER', true),
            DigikalaProvider::class => env('DIGIKALA_SERVICE_PROVIDER', true),
            TimcheProvider::class => env('TIMCHE_SERVICE_PROVIDER', true),
        ];

        $result = [];

        foreach ($serviceProviders as $serviceProvider => $status) {
            if ($status) {
                $result[] = app()->make($serviceProvider)->products($lat, $long);
            }
        }

        return $result;
    }

    /**
     * @return void
     */
    private function init(): void
    {
        /** @var AuthTokenRepositoryInterface $authTokenRepositoryInterface */
        $authTokenRepositoryInterface = app()->make(AuthTokenRepositoryInterface::class);

        /** @var UserRepositoryInterface $userRepositoryInterface */
        $userRepositoryInterface = app()->make(UserRepositoryInterface::class);

        /** @var StoreRepositoryInterface $storeRepositoryInterface */
        $storeRepositoryInterface = app()->make(StoreRepositoryInterface::class);

        $token = \request()->bearerToken();
        $authToken = $authTokenRepositoryInterface->findByToken($token);

        $this->authUser = $userRepositoryInterface->find($authToken->user_id);
        $this->store = $storeRepositoryInterface->getStoreBySellerId($authToken->user_id);
    }
}
