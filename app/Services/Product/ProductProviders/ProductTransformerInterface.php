<?php

namespace App\Services\Product\ProductProviders;

interface ProductTransformerInterface
{
    /**
     * @return string
     */
    public function title(): string;

    /**
     * @return string
     */
    public function description(): string;

    /**
     * @return string
     */
    public function price(): string;
}
