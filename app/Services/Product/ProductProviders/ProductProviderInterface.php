<?php

namespace App\Services\Product\ProductProviders;

use Illuminate\Http\Resources\Json\JsonResource;

interface ProductProviderInterface
{
    /**
     * @param string $lat
     * @param string $long
     *
     * @return JsonResource
     */
    public function products(string $lat, string $long): JsonResource;
}
