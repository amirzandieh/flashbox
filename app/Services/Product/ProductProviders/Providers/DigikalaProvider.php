<?php

namespace App\Services\Product\ProductProviders\Providers;

use App\Http\Resources\ProductProviderResources\DigikalaProviderResource;
use App\Services\Product\ProductProviders\ProductProviderInterface;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class DigikalaProvider implements ProductProviderInterface
{
    /**
     * @param string $lat
     * @param string $long
     *
     * @return JsonResource
     */
    public function products(string $lat, string $long): JsonResource
    {
        $products = [
            [
                'name' => Str::random(20),
                'desc' => Str::random(200),
                'price' => 3000
            ],
            [
                'name' => Str::random(20),
                'desc' => Str::random(200),
                'price' => 4000
            ],
            [
                'name' => Str::random(20),
                'desc' => Str::random(200),
                'price' => 5000
            ]
        ];

        return (new DigikalaProviderResource($products));
    }
}
