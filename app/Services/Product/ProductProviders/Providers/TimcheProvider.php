<?php

namespace App\Services\Product\ProductProviders\Providers;

use App\Http\Resources\ProductProviderResources\TimcheProviderResource;
use App\Services\Product\ProductProviders\ProductProviderInterface;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class TimcheProvider implements ProductProviderInterface
{
    /**
     * @param string $lat
     * @param string $long
     *
     * @return JsonResource
     */
    public function products(string $lat, string $long): JsonResource
    {
        $products = [
            [
                'name' => Str::random(20),
                'desc' => Str::random(200),
                'price' => 3000
            ],
            [
                'name' => Str::random(20),
                'desc' => Str::random(200),
                'price' => 4000
            ],
            [
                'name' => Str::random(20),
                'desc' => Str::random(200),
                'price' => 5000
            ]
        ];

        return (new TimcheProviderResource($products));
    }
}
