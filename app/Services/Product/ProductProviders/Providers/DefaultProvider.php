<?php

namespace App\Services\Product\ProductProviders\Providers;

use App\Http\Resources\ProductProviderResources\DefaultProviderResource;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Repositories\Interfaces\StoreRepositoryInterface;
use App\Services\Product\ProductProviders\ProductProviderInterface;
use Illuminate\Http\Resources\Json\JsonResource;

class DefaultProvider implements ProductProviderInterface
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepository;

    /**
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository, StoreRepositoryInterface $storeRepository)
    {
        $this->productRepository = $productRepository;
        $this->storeRepository = $storeRepository;
    }

    /**
     * @param string $lat
     * @param string $long
     *
     * @return JsonResource
     */
    public function products(string $lat, string $long): JsonResource
    {
        $storeIds = $this->storeRepository->getStoreIdsWithGeoParams($lat, $long);

        $products = $this->productRepository->getProducts($storeIds);

        return (new DefaultProviderResource($products));
    }
}
