<?php

namespace App\Services\Auth;

use App\Services\Auth\LoginServiceProviders\LoginServiceProviderFactory;
use Illuminate\Http\Request;

class AuthService
{
    /**
     * @var LoginServiceProviderFactory
     */
    protected $loginServiceProviderFactory;

    /**
     * @param LoginServiceProviderFactory $loginServiceProviderFactory
     */
    public function __construct(LoginServiceProviderFactory $loginServiceProviderFactory)
    {
        $this->loginServiceProviderFactory = $loginServiceProviderFactory;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function loginProcess(Request $request): array
    {
        $email = $request->get('email');
        $service = $request->get('method') ?? 'default';

        $loginServiceProvider = $this->loginServiceProviderFactory->make($service);

        return $loginServiceProvider->execute($email);
    }
}
