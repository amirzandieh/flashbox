<?php

namespace App\Services\Auth\LoginServiceProviders;

interface LoginServiceProviderInterface
{
    /**
     * @param string $email
     *
     * @return array
     */
    public function execute(string $email): array;
}
