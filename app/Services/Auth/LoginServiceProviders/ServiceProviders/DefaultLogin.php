<?php

namespace App\Services\Auth\LoginServiceProviders\ServiceProviders;

use App\Repositories\Interfaces\AuthTokenRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\Auth\LoginServiceProviders\AbstractLoginServiceProvider;
use App\Services\Auth\LoginServiceProviders\LoginServiceProviderInterface;

class DefaultLogin extends AbstractLoginServiceProvider implements LoginServiceProviderInterface
{
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;
    /**
     * @var AuthTokenRepositoryInterface
     */
    protected $authTokenRepository;

    /**
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository, AuthTokenRepositoryInterface  $authTokenRepository)
    {
        $this->userRepository = $userRepository;
        $this->authTokenRepository = $authTokenRepository;
    }

    /**
     * @param string $email
     *
     * @return array
     */
    public function execute(string $email): array
    {
        $user = $this->userRepository->getUserViaEmail($email);

        $token = tokenGenerator();
        $this->authTokenRepository->create([
            "user_id" => $user->id,
            "token" => $token
        ]);

        return [
            'token' => $token,
            'user' => $user
        ];
    }
}
