<?php

namespace App\Services\Auth\LoginServiceProviders\ServiceProviders;

use App\Services\Auth\LoginServiceProviders\AbstractLoginServiceProvider;
use App\Services\Auth\LoginServiceProviders\LoginServiceProviderInterface;

class GithubLogin extends AbstractLoginServiceProvider implements LoginServiceProviderInterface
{
    /**
     * @param string $email
     *
     * @return array
     */
    public function execute(string $email): array
    {
        // TODO: Implement execute() method.
    }
}
