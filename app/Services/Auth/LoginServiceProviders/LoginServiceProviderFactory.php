<?php

namespace App\Services\Auth\LoginServiceProviders;

use App\Services\Auth\LoginServiceProviders\ServiceProviders\DefaultLogin;

class LoginServiceProviderFactory
{
    /**
     * @param string $service
     *
     * @return LoginServiceProviderInterface
     */
    public function make(string $service): LoginServiceProviderInterface
    {
        try {
            $class = "App\Services\Auth\LoginServiceProviders\ServiceProviders\*class*Login";
            $class = str_replace('*class*', ucfirst($service), $class);
            $instance = app()->make($class);
        } catch (\Exception $exception) {
            $instance = app()->make(DefaultLogin::class);
        }

        return $instance;
    }
}
