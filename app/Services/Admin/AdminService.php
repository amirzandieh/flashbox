<?php

namespace App\Services\Admin;

use App\Enums\RolesEnum;
use App\Models\User;
use App\Repositories\Interfaces\RoleRepositoryInterface;
use App\Repositories\Interfaces\StoreRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\Geocode\Facades\Geocode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AdminService
{
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var RoleRepositoryInterface
     */
    protected $roleRepository;

    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepository;

    /**
     * @param UserRepositoryInterface $userRepository
     * @param RoleRepositoryInterface $roleRepository
     */
    public function __construct(UserRepositoryInterface $userRepository,
                                RoleRepositoryInterface $roleRepository,
                                StoreRepositoryInterface $storeRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->storeRepository = $storeRepository;
    }

    /**
     * @param Request $request
     *
     * @return User
     */
    public function sellerStore(Request $request): User
    {
        $email = $request->get('email');
        $password = $request->get('password');
        $address = $request->get('address');

        $geocode = Geocode::geocode($address);

        $user = DB::transaction(function () use (&$email, &$password, &$geocode, &$address) {
            $seller = $this->userRepository->create([
                'email' => $email,
                'password' => getHashOfString($password)
            ]);
            $sellerRole = $this->roleRepository->getRoleByName(RolesEnum::SELLER);
            $seller->roles()->attach($sellerRole->id);

            $this->storeRepository->create([
                'title' => Str::random(6),
                'description' => Str::random(50),
                'seller_id' => $seller->id,
                'lat' => $geocode['lat'],
                'long' => $geocode['long'],
                'address' => $address,
            ]);

            return $seller;
        });

        return $user;
    }
}
