<?php

namespace App\Services\Geocode\Providers;

use App\Services\Geocode\GeocodeInterface;
use OpenCage\Geocoder\Geocoder;

class OpenCageService implements GeocodeInterface
{
    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var Geocoder
     */
    protected $geocoder;

    public function __construct()
    {
        $this->apiKey = env('OPEN_CAGE_API_KEY', '3e2be49abbe94bc4bbffe64a342c1711');
        $this->init();
    }

    public function geocode(string $address): array
    {
        $geocode = $this->geocoder->geocode($address)['results'];
        $result = $geocode[0] ?? [];

        if ($result) {
            $lat = $result['geometry']['lat'];
            $long = $result['geometry']['lng'];
        } else {
            $lat = 0;
            $long = 0;
        }

        return [
            'lat' => $lat,
            'long' => $long
        ];
    }

    /**
     * @return void
     */
    private function init(): void
    {
        $this->geocoder = new Geocoder($this->apiKey);
    }
}
