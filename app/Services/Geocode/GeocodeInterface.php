<?php

namespace App\Services\Geocode;

interface GeocodeInterface
{
    /**
     * @param string $address
     *
     * @return array
     */
    public function geocode(string $address): array;
}
