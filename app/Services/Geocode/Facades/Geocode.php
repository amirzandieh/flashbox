<?php

namespace App\Services\Geocode\Facades;

use Illuminate\Support\Facades\Facade;

class Geocode extends Facade
{
    public static function getFacadeAccessor()
    {
        return "GeocodeServiceFacade";
    }
}
