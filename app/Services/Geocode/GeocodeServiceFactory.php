<?php

namespace App\Services\Geocode;

use App\Services\Geocode\Providers\OpenCageService;
use InvalidArgumentException;

class GeocodeServiceFactory
{
    protected $service = 'OpenCage';

    public function __construct(string $service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function make()
    {
        $class = "App\Services\Geocode\Providers\\" . ucfirst($this->service) . "Service";

        try {
            $instance =  app()->make($class);
        } catch (\Exception $exception) {
            $instance = app()->make(OpenCageService::class);
        }

        return $instance;
    }
}
