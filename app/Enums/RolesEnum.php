<?php

namespace App\Enums;

class RolesEnum
{
    public const ADMIN = 'admin';
    public const CUSTOMER = 'customer';
    public const SELLER = 'seller';
}
