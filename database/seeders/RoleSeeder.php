<?php

namespace Database\Seeders;

use App\Enums\RolesEnum;
use App\Repositories\Interfaces\RoleRepositoryInterface;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(RoleRepositoryInterface $roleRepository)
    {
        $roles = [
            RolesEnum::ADMIN,
            RolesEnum::CUSTOMER,
            RolesEnum::SELLER
        ];

        foreach ($roles as $role) {
            $roleRepository->createRole($role, "This is $role");
        }
    }
}
