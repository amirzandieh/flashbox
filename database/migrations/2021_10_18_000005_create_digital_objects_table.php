<?php

use App\Constants\Tables;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDigitalObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Tables::DIGITAL_OBJECTS, function (Blueprint $table) {
            $table->id();

            $table->string('title');
            $table->string('description');

            $table->unsignedInteger('store_id');

            $table->string('price');
            $table->string('tax');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Tables::DIGITAL_OBJECTS);
    }
}
