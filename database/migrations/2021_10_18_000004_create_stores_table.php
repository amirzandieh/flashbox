<?php

use App\Constants\Tables;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Tables::STORES, function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');
            $table->string('description')->nullable();

            $table->string('phone')->nullable();

            $table->text('address')->nullable();

            $table->string('lat');
            $table->string('long');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Tables::STORES);
    }
}
